<?php
/**
 * UNIT test UUID
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\Utils\Text;

class TextTest extends TestCase
{
	public function testCanGetUuid()
	{
		$uuid = Text::uuid();

		$this->assertTrue(preg_match('#^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$#', $uuid) === 1);
	}
}