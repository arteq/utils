<?php
/**
 * UNIT test for file lock
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\Utils\Lock;

class LockTest extends TestCase
{
	const PATH = __DIR__.'/../tmp/file_lock';
	private $lock;

	/* ====================================================================== */
	
	public function setUp()
	{
		$lock = new Lock(self::PATH);
		$this->lock = $lock;
	}

	/* ====================================================================== */
	
	public function tearDown()
	{
		@unlink(self::PATH);
	}

	/* ====================================================================== */
	
	public function testCanSetLock()
	{
		$locked = $this->lock->setLock();

		$this->assertTrue($locked);
		$this->assertTrue(is_file(self::PATH));
		$this->assertTrue($this->lock->isLocked());
	}

	/* ====================================================================== */
	
	public function testCanSetLockWithData()
	{
		$data = [
			'end' => date('Y-m-d H:i:s', strtotime('+1 hour')),
			'extra' => 'extra info',
		];
		
		$locked = $this->lock->setLock($data);
		$this->assertTrue($locked);
		$this->assertTrue(is_file(self::PATH));
		$this->assertTrue($this->lock->isLocked());

		$lockData = [];
		$this->lock->isLocked($lockData);
		$this->assertArrayHasKey('begin', $lockData);
		$this->assertArrayHasKey('end', $lockData);
		$this->assertArrayHasKey('extra', $lockData);
	}

	/* ====================================================================== */
	
	public function testCanUnlockStalled()
	{
		$data = [
			'end' => date('Y-m-d H:i:s', strtotime('-1 hour')),
		];
		
		$locked = $this->lock->setLock($data);
		$this->assertTrue($locked);
		$this->assertTrue(is_file(self::PATH));
		$this->assertFalse($this->lock->isLocked());
	}

	/* ====================================================================== */
	
	public function testCanSetCustomTimeout()
	{
		$data = [
			'timeout' => '+1 hour'
		];
		
		$locked = $this->lock->setLock($data);
		$this->assertTrue($locked);
		$this->assertTrue(is_file(self::PATH));
		$this->assertTrue($this->lock->isLocked());
	}

	/* ====================================================================== */
	
	public function testCanUlock()
	{
		$this->lock->setLock();
		$this->assertTrue(is_file(self::PATH));
		
		$this->lock->unlock();
		$this->assertFalse(is_file(self::PATH));		
	}

	/* ====================================================================== */
	
	public function testConcurentLock()
	{
		$lock1 = new Lock(self::PATH);
		$lock1->setLock(['timeout' => '+1 hour']);

		$lock2 = new Lock(self::PATH);
		$this->assertTrue( $lock2->isLocked() );
	}
}