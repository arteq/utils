<?php
/**
 * Semaphore file lock
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\Utils;

class Lock
{
	/**
	 * @var string 
	 */ 
	private $path;

	/* ====================================================================== */
	
	/**
	 * @param string $path
	 */ 
	public function __construct($path)
	{
		$this->path = $path;
	}

	/* ====================================================================== */
	
	/**
	 * Set file lock with optional options
	 * 
	 * @param array $options possible keys: begin,end,timeout
	 * @return bool
	 */ 
	public function setLock($options = [])
	{
		$optionsDefault = [
			'begin' => date('Y-m-d H:i:s'),
		];

		if (isset($options['timeout']))
		{
			$optionsDefault['end'] = date('Y-m-d H:i:s', strtotime($options['timeout']));
		}

		$options = array_merge($optionsDefault, $options);

		if (file_put_contents($this->path, json_encode($options)) > 0)
			return true;
		else
			return false;
	}

	/* ====================================================================== */
	
	/**
	 * Remove lock, delete lock file
	 */ 
	public function unlock()
	{
		@unlink($this->path);
	}

	/* ====================================================================== */
	
	/**
	 * Check if there is a lock set, and is still valid
	 * 
	 * @param array $data when set will be set with extra info from lock file
	 * @return bool
	 */  
	public function isLocked(&$data = [])
	{
		// clear cache of existing files
		clearstatcache();

		if (!is_file($this->path))
			return false;

		$data = file_get_contents($this->path);
		$data = json_decode($data, true);

		if (isset($data['end']) && ($data['end'] < date('Y-m-d H:i:s')) )
		{
			@unlink($this->path);
			return false;
		}

		return true;
	}
}