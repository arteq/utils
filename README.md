# Utils - pomocnicze funkcje

## Instalacja
```sh 
$ composer require arteq/utils
```

## Lock
Tworzy na dysku plik blokady wkazujący, że jakiś proces jest aktualnie uruchomiony i nie należy uruchamiać go ponownie. Przydatne np w przypadku zadań uruchamianych z cron'a, które mogą trwać dłużej niż zastosowany interwał crona a ich nałożenie się na siebie jest niepożądane.

```php
<?php 

use ArteQ\Utils\Lock;

require __DIR__.'/../vendor/autoload.php';

// create lock
$lock = new Lock('/path/to/file.lock');

// check if is already locked?
if ($lock->isLocked())
{
	echo "Locked";
	exit();
}

// set lock:
// 1. permanent lock, no expiration date
$lock->setLock();

// 2. expiration provided as strtotime expression
// $lock->setLock(['timeout' => '+1 hour']);

// 3. expiration provided as fixed date
// $lock->setLock(['end' => '2019-01-01 12:00:00']);

// do something time consuming...

// then unlock when done
$lock->unlock();
```


## Text
Pomocnicze funkcje operujące na ciągach tekstowych:
* generowanie unikalnych UUID v4

```php
<?php 

use ArteQ\Utils\Text;

require __DIR__.'/../vendor/autoload.php';

echo Text::uuid();
```